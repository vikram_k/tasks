package tasks

import (
	"context"
	"database/sql"
	"github.com/elgris/sqrl"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	sqlpx "github.com/srikrsna/sqlx/proto"
	"go.saastack.io/chaku/errors"
	"go.saastack.io/idutil"
	"go.saastack.io/userinfo"
	"google.golang.org/genproto/protobuf/field_mask"

	"go.saastack.io/task/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	employee "go.saastack.io/employee/pb"
	project "go.saastack.io/project/pb"
)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
)

type tasksServer struct {
	db *sql.DB
	employeeCli employee.EmployeesClient
	taskStore pb.TaskStore
	taskBLoC  pb.TasksServiceTaskServerBLoC
	parentServer pb.ParentServiceClient
	*pb.TasksServiceTaskServerCrud
}

func NewTasksServer(
	db *sql.DB,
	employeeCLi employee.EmployeesClient,
	taskSt pb.TaskStore,
	parentServer pb.ParentServiceClient,

) pb.TasksServer {
	r := &tasksServer{
		db: db,
		parentServer: parentServer,
		taskStore: taskSt,
		employeeCli: employeeCLi,
	}

	taskSC := pb.NewTasksServiceTaskServerCrud(taskSt, r)
	r.TasksServiceTaskServerCrud = taskSC

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *tasksServer) CreateTaskBLoC(ctx context.Context, in *pb.CreateTaskRequest) error {
	if _, err := s.parentServer.ValidateParent(ctx, &pb.ValidateParentRequest{Id: in.GetParent()}); err != nil {
		return err
	}

	// Check if task title already not exists
	condition := pb.TaskAnd{pb.TaskParentEq{Parent: idutil.GetId(in.GetParent())}, pb.TaskTitleEq{Title: in.GetTask().GetTitle()}}

	_, err := s.taskStore.GetTask(ctx, []string{string(pb.Task_Id)}, condition)
	if err != nil {
		if err == errors.ErrNotFound {
			return nil
		}
		return err
	}

	return errors.ErrObjIdExist
}

func (s *tasksServer) GetTaskBLoC(ctx context.Context, in *pb.GetTaskRequest) error {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	return nil
}

func (s *tasksServer) UpdateTaskBLoC(ctx context.Context, in *pb.UpdateTaskRequest) error {
	//Validate Update mask
	for _, m := range in.GetUpdateMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	// Check if task exists or not
	condition := pb.TaskAnd{pb.TaskParentEq{Parent: idutil.GetParent(in.GetTask().GetId())}, pb.TaskTitleEq{Title: in.GetTask().GetTitle()}, pb.TaskIdNotEq{Id: in.GetTask().GetId()}}

	_, err := s.taskStore.GetTask(ctx, []string{string(pb.Task_Id)}, condition)
	if err != nil {
		if err == errors.ErrNotFound {
			return nil
		}
		return err
	}

	return errors.ErrObjIdExist
}

func (s *tasksServer) DeleteTaskBLoC(ctx context.Context, in *pb.DeleteTaskRequest) error {
	return nil
}

func (s *tasksServer) ListTaskBLoC(ctx context.Context, in *pb.ListTaskRequest) (pb.TaskCondition, error) {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	projectId := in.Parent
	cond := pb.TaskAnd{
		pb.TaskParentEq{Parent: idutil.GetId(projectId)}}

	if in.Filters != nil {
		// filter by the title
		if in.Filters.Title != "" {
			cond = append(cond, pb.TaskTitleILike{Title: "%" + in.Filters.Title + "%"})
		}
		// filter by due date after
		if in.Filters.DueDateAfter != "" {
			cond = append(cond, pb.TaskDueTimeGt{DueTime: in.Filters.DueDateAfter})
		}
		//filter by due date before
		if in.Filters.DueDateBefore != nil {
			cond = append(cond, pb.TaskDueTimeLt{DueTime: in.Filters.DueDateBefore})
		}
		// filter by status
		if in.Filters.Status != nil {
			cond = append(cond, pb.TaskTaskStatusEq{TaskStatus: in.Filters.Status})
		}
	}

	return cond, nil
}

// These functions are not implemented by CRUDGen, needed to be implemented

func (s *tasksServer) UpdateAssignedEmployee(ctx context.Context, in *pb.UpdateAssignedEmployeeRequest) (*pb.Task, error) {
	if err := in.Validate(); err!=nil {
		return nil, err
	}

	task, err := s.taskStore.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.GetId()})
	if err!=nil {
		return nil, err
	}

	meta := &employee.MetaInfo{}
	store := employee.EmployeeStore{}
	_, err = store.GetEmployee(ctx, []string{}, employee.EmployeeIdEq{Id: in.GetId()}, meta)
	if err!=nil {
		if err == errors.ErrNotFound {
			return nil, errors.ErrInvalidField
		}
		return nil, err
	}

	task.Assignee = in.GetEmpId()

	err = s.taskStore.UpdateTask(ctx, task, []string{}, pb.TaskIdEq{Id:  in.GetId()})
	if err!=nil {
		return nil, err
	}

	return task, nil
}

func (s *tasksServer) TasksReport(ctx context.Context, in *pb.TasksReportRequest) (*pb.TasksReportResponse, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	sq := sqrl.Select("date(task.created_on::date)", "COUNT(*)").From("saastack_task_v1.task").Where(sqrl.And{
		sqrl.GtOrEq{"task.created_on": sqlpx.Timestamp(in.TimeRange.StartTime},
		sqrl.LtOrEq{"task.created_on": sqlpx.Timestamp(in.TimeRange.EndTime)},
		sqrl.Eq{"task.status": pb.TaskStatus()},
	}).GroupBy("DATE(task.created_on)").OrderBy("DATE(task.created_on)")

	args := []interface{}{
		sqlpx.Timestamp(in.TimeRange.StartTime),
		sqlpx.Timestamp(in.TimeRange.EndTime),
		idutil.GetId(in.Parent),
	}

	query, args, err := sq.PlaceholderFormat(sqrl.Dollar).ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	list := make([]*pb.ReportDay)

	for rows.Next() {
		rep := &pb.ReportDay{}
		if err := rows.Scan(
			&rep.Date ,
			&rep.TotalTasks); err != nil {
			return nil, err
		}
		list = append(list, rep)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return list, nil

}

type parentServiceServer struct {
	proCli project.ProjectsClient
}

//NewParentServiceServer returns a ParentServiceServer implementation with core business logic
func NewParentServiceServer(proCli project.ProjectsClient) pb.ParentServiceServer {
	return &parentServiceServer{
		proCli: proCli,
	}

}


func (s *parentServiceServer) ValidateParent(ctx context.Context, in *pb.ValidateParentRequest) (*pb.ValidateParentResponse, error) {
	if skip := userinfo.SkipParent(ctx); !skip {
		if _, err := s.proCli.GetProject(ctx, &project.GetProjectRequest{Id: in.GetId(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}}); err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}
	}

	return &pb.ValidateParentResponse{Valid: true}, nil

}
