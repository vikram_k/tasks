package pb

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/chaku/errors"
	"go.saastack.io/idutil"
	"go.saastack.io/protos/types"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type TasksServiceTaskServerCrud struct {
	store TaskStore
	bloc  TasksServiceTaskServerBLoC
}

type TasksServiceTaskServerBLoC interface {
	CreateTaskBLoC(context.Context, *CreateTaskRequest) error

	GetTaskBLoC(context.Context, *GetTaskRequest) error

	UpdateTaskBLoC(context.Context, *UpdateTaskRequest) error

	DeleteTaskBLoC(context.Context, *DeleteTaskRequest) error

	ListTaskBLoC(context.Context, *ListTaskRequest) (TaskCondition, error)
}

func NewTasksServiceTaskServerCrud(s TaskStore, b TasksServiceTaskServerBLoC) *TasksServiceTaskServerCrud {
	return &TasksServiceTaskServerCrud{store: s, bloc: b}
}

func (s *TasksServiceTaskServerCrud) CreateTask(ctx context.Context, in *CreateTaskRequest) (*Task, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.CreateTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if idutil.GetPrefix(in.Task.Id) != in.Task.GetPrefix() {
		in.Task.Id = in.Parent
	}

	ids, err := s.store.CreateTasks(ctx, in.Task)
	if err != nil {
		return nil, err
	}

	in.Task.Id = ids[0]

	return in.GetTask(), nil
}

func (s *TasksServiceTaskServerCrud) UpdateTask(ctx context.Context, in *UpdateTaskRequest) (*Task, error) {

	mask := s.GetViewMask(in.UpdateMask)
	if len(mask) == 0 {
		return nil, status.Error(codes.InvalidArgument, "cannot send empty update mask")
	}

	if err := in.GetTask().Validate(mask...); err != nil {
		return nil, err
	}

	err := s.bloc.UpdateTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.UpdateTask(ctx,
		in.Task, mask,
		TaskIdEq{Id: in.Task.Id},
	); err != nil {
		return nil, err
	}

	updatedTask, err := s.store.GetTask(ctx, []string{},
		TaskIdEq{
			Id: in.GetTask().GetId(),
		},
	)
	if err != nil {
		return nil, err
	}

	return updatedTask, nil
}

func (s *TasksServiceTaskServerCrud) GetTask(ctx context.Context, in *GetTaskRequest) (*Task, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.GetTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	res, err := s.store.GetTask(ctx, mask, TaskIdEq{Id: in.Id})
	if err != nil {
		if err == errors.ErrNotFound {
			return nil, status.Error(codes.NotFound, "Task not found")
		}
		return nil, err
	}

	return res, nil
}

func (s *TasksServiceTaskServerCrud) ListTask(ctx context.Context, in *ListTaskRequest) (*ListTaskResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	condition, err := s.bloc.ListTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	page, err := s.GetPagedCondition(ctx, in.First, in.After, in.Last, in.Before)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	return s.ListWithPagination(ctx, page, condition, mask)
}

func (s *TasksServiceTaskServerCrud) ListWithPagination(ctx context.Context, page *CursorBasedPagination, condition TaskCondition, viewMask []string) (*ListTaskResponse, error) {

	list, err := s.store.ListTasks(ctx,
		viewMask,
		condition,
		page,
	)
	if err != nil {
		return nil, err
	}

	res := &ListTaskResponse{
		PageInfo: &types.PageInfo{},
	}

	for _, it := range list {

		res.Nodes = append(res.Nodes, &TaskNode{Position: it.Id, Node: it})
	}
	res.PageInfo.HasPrevious = page.HasPrevious
	res.PageInfo.HasNext = page.HasNext
	if len(list) > 0 {
		res.PageInfo.StartCursor = list[0].Id
		res.PageInfo.EndCursor = list[len(list)-1].Id
	}

	return res, nil

}

func (s *TasksServiceTaskServerCrud) GetPagedCondition(ctx context.Context, first uint32, after string, last uint32, before string) (*CursorBasedPagination, error) {

	page := &CursorBasedPagination{}
	flag := false

	if first != 0 {
		flag = true
		page = &CursorBasedPagination{
			Cursor:   idutil.GetId(after),
			Limit:    int(first),
			UpOrDown: false,
		}

	} else if last != 0 {
		flag = true
		page = &CursorBasedPagination{
			Cursor:   idutil.GetId(before),
			Limit:    int(last),
			UpOrDown: true,
		}
	}
	if !flag {
		return nil, status.Error(codes.InvalidArgument, "either after-first or before-last should be set in request")
	}

	return page, nil
}

func (s *TasksServiceTaskServerCrud) DeleteTask(ctx context.Context, in *DeleteTaskRequest) (*empty.Empty, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.DeleteTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.DeleteTask(ctx, TaskIdEq{Id: in.Id}); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *TasksServiceTaskServerCrud) GetViewMask(mask *field_mask.FieldMask) []string {
	if mask == nil || mask.GetPaths() == nil {
		return []string{}
	}
	return mask.GetPaths()
}
