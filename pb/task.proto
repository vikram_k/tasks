syntax = "proto3";

package saastack.tasks.v1;

option go_package = "pb";

import "annotations/annotations.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/field_mask.proto";
import "validate/validate.proto";
import "validate/chaku.proto";
import "types/types.proto";
import "pehredaar/pehredaar.proto";
import "eventspush/push.proto";
import "schema/schema.proto";
import "google/protobuf/timestamp.proto";

service Tasks {

    // CreateTask creates new task.
    rpc CreateTask (CreateTaskRequest) returns (Task) {
        option (google.api.http) = {
            post: "/v1/tasks"
            body: "*"
        };
        option (pehredaar.paths) = {
            resource: "parent"
        };
        option (eventspush.event) = {
            push: true
        };
        option (graphql.schema) = {
            mutation : "createTask"
        };
    }

    // GetTask returns the task by its unique id.
    rpc GetTask (GetTaskRequest) returns (Task) {
        option (google.api.http) = {
            get: "/v1/tasks/{id}"
        };
        option (pehredaar.paths) = {
            resource: "id"
        };
        option (graphql.schema) = {
            query : "task"
        };
    }

    // DeleteTask will delete the task from the system by Id.
    // This will be a soft delete from the system
    rpc DeleteTask (DeleteTaskRequest) returns (google.protobuf.Empty) {
        option (google.api.http) = {
            delete: "/v1/tasks/{id}"
        };
        option (pehredaar.paths) = {
            resource: "id"
        };
        option (eventspush.event) = {
            push: true
        };
        option (graphql.schema) = {
            mutation : "deleteTask"
        };
    }

    // UpdateTask will update the task identified by its task id.
    // Update Task uses Field Mask to update specific properties of task object
    rpc UpdateTask (UpdateTaskRequest) returns (Task) {
        option (google.api.http) = {
            put: "/v1/tasks/{task.id}"
            body: "*"
        };
        option (pehredaar.paths) = {
            resource: "task.id"
        };
        option (eventspush.event) = {
            push: true
        };
        option (graphql.schema) = {
            mutation : "updateTask"
        };
    }

    // ListTask lists all the Task(s)
    rpc ListTask (ListTaskRequest) returns (ListTaskResponse) {
        option (google.api.http) = {
            get : "/v1/tasks"
        };
        option (pehredaar.paths) = {
            resource: "parent"
        };
        option (graphql.schema) = {
            query : "tasks"
        };
    }

    rpc UpdateAssignedEmployee(UpdateAssignedEmployeeRequest) returns (Task){
        option (google.api.http) = {
            patch: "/v1/tasks/{id}/assignee"
            body: "*"
        };
        option (pehredaar.paths) = {
            resource: "id"
        };
        option (eventspush.event) = {
            push: true
        };
        option (graphql.schema) = {
            mutation : "updateAssignee"
        };
    }

    rpc TasksReport(TasksReportRequest) returns (TasksReportResponse){
        option (google.api.http) = {
            get : "/v1/tasks/report"
        };
        option (pehredaar.paths) = {
            resource: "parent"
            resource:"emp_id"
        };
        option (graphql.schema) = {
            query : "reportByDate"
        };
    }


}

message Task {

    option (chaku.root) = true;
    option (chaku.prefix) = 'tas';

    string id = 1;

	  // fields

    string title = 2 [(validate.rules).string = {min_len : 1, max_len : 75}];
    google.protobuf.Timestamp due_time = 3 [(validate.rules).timestamp.required = true];
    NotificationType notification_type = 4 [(validate.rules).enum.defined_only = true];
    string assignee = 5;
    TaskStatus task_status = 6 [(validate.rules).enum.defined_only = true];
    bool priority = 7;

    google.protobuf.Timestamp created_on = 8 [(validate.rules).timestamp.required = true];
}

enum NotificationType {
    SMS = 0;
    EMAIL = 1;
}

enum TaskStatus {
    COMPLETED = 0;
    INCOMPLETE = 1;
    UNSPECIFIED  = 2;
}

message CreateTaskRequest {
    string parent = 1 [(validate.rules).string.min_len = 3];
    Task task = 2 [(validate.rules).message.required = true];
}

message GetTaskRequest {
    string id = 1 [(validate.rules).string.min_len = 3];
    google.protobuf.FieldMask view_mask = 2;
}

message DeleteTaskRequest {
    string id = 1 [(validate.rules).string.min_len = 3];
}

message UpdateTaskRequest {
    Task task = 1 [(validate.rules).message.required = true];
    google.protobuf.FieldMask update_mask = 2;
}

message ListTaskRequest {

    // Parent is a fully qualified string that contains information about the
    // owner in hierarchical manner group/location/business (required)
    string parent = 1 [(validate.rules).string.min_len = 1];

    TaskListFilters filters = 2;
    uint32 first = 3;
    string after = 4;
    uint32 last = 5;
    string before = 6;
    bool high_priority_first = 7;
    google.protobuf.FieldMask view_mask = 8;
}

message TaskListFilters {
    string title = 1;
    TaskStatus status = 2;
    google.protobuf.Timestamp due_date_after = 3;
    google.protobuf.Timestamp due_date_before = 4;
}

message ListTaskResponse {

    repeated TaskNode nodes = 1;
    saastack.types.PageInfo page_info = 2;
}

message TaskNode {
    string position = 1;
    Task node = 2;
}

message UpdateAssignedEmployeeRequest {
    // task id
    string id = 1 [(validate.rules).string.min_len = 3];
    //employee id
    string emp_id = 2 [(validate.rules).string.min_len = 3];
}

message TasksReportRequest {
    string parent = 1 [(validate.rules).string.min_len = 1];
    string emp_id = 2;
    types.Timeslot time_range = 3 [(validate.rules).message.required = true];

}

message UpdateTaskLog {
    Task old_task = 1;
    Task new_task = 2;
    google.protobuf.FieldMask update_mask = 3;
}

message TasksReportResponse {
    repeated ReportDay report_day = 1;
    int64 total_tasks = 2;
}

message ReportDay {
    string date = 1;
    int64 total_tasks  = 2;
}

////////////////////------------------

option (pehredaar.module_roles).module_role = {
    module_role_name: "Admin"
    display_name: "Admin"
    pattern: "{parent}**/.*"
};

option (pehredaar.module_roles).module_role = {
    module_role_name: "Manager"
    display_name: "Manager"
    rpc: "GetTask"
    rpc: "ListTask"
    rpc: "UpdateTask"
    rpc: "UpdateAssignedEmployee"

};

/////////////////-----------------

service ParentService {
    rpc ValidateParent (ValidateParentRequest) returns (ValidateParentResponse);
}

message ValidateParentRequest {
    option (graphql.skip) = true;

    string id = 1;
}

message ValidateParentResponse {
    option (graphql.skip) = true;

    bool valid = 1;
}
