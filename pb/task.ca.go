package pb

import (
	"context"

	. "github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"go.saastack.io/protoc-gen-caw/convert"
	"go.uber.org/cadence/activity"
	"go.uber.org/cadence/workflow"
)

var (
	_ = Empty{}
	_ = convert.JsonB{}
)

const (
	TasksCreateTaskActivity             = "/saastack.tasks.v1.Tasks/CreateTask"
	TasksGetTaskActivity                = "/saastack.tasks.v1.Tasks/GetTask"
	TasksDeleteTaskActivity             = "/saastack.tasks.v1.Tasks/DeleteTask"
	TasksUpdateTaskActivity             = "/saastack.tasks.v1.Tasks/UpdateTask"
	TasksListTaskActivity               = "/saastack.tasks.v1.Tasks/ListTask"
	TasksUpdateAssignedEmployeeActivity = "/saastack.tasks.v1.Tasks/UpdateAssignedEmployee"
	TasksTasksReportActivity            = "/saastack.tasks.v1.Tasks/TasksReport"
)

func RegisterTasksActivities(cli TasksClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *CreateTaskRequest) (*Task, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.CreateTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksCreateTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTaskRequest) (*Task, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.GetTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksGetTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *DeleteTaskRequest) (*Empty, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.DeleteTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksDeleteTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateTaskRequest) (*Task, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.UpdateTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksUpdateTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskRequest) (*ListTaskResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ListTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateAssignedEmployeeRequest) (*Task, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.UpdateAssignedEmployee(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksUpdateAssignedEmployeeActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *TasksReportRequest) (*TasksReportResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.TasksReport(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksTasksReportActivity},
	)
}

// TasksActivitiesClient is a typesafe wrapper for TasksActivities.
type TasksActivitiesClient struct {
}

// NewTasksActivitiesClient creates a new TasksActivitiesClient.
func NewTasksActivitiesClient(cli TasksClient) TasksActivitiesClient {
	RegisterTasksActivities(cli)
	return TasksActivitiesClient{}
}

func (ca *TasksActivitiesClient) CreateTask(ctx workflow.Context, in *CreateTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksCreateTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) GetTask(ctx workflow.Context, in *GetTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksGetTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) DeleteTask(ctx workflow.Context, in *DeleteTaskRequest) (*Empty, error) {
	future := workflow.ExecuteActivity(ctx, TasksDeleteTaskActivity, in)
	var result Empty
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) UpdateTask(ctx workflow.Context, in *UpdateTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksUpdateTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTask(ctx workflow.Context, in *ListTaskRequest) (*ListTaskResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskActivity, in)
	var result ListTaskResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) UpdateAssignedEmployee(ctx workflow.Context, in *UpdateAssignedEmployeeRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksUpdateAssignedEmployeeActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) TasksReport(ctx workflow.Context, in *TasksReportRequest) (*TasksReportResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksTasksReportActivity, in)
	var result TasksReportResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

const (
	ParentServiceValidateParentActivity = "/saastack.tasks.v1.ParentService/ValidateParent"
)

func RegisterParentServiceActivities(cli ParentServiceClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ValidateParent(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ParentServiceValidateParentActivity},
	)
}

// ParentServiceActivitiesClient is a typesafe wrapper for ParentServiceActivities.
type ParentServiceActivitiesClient struct {
}

// NewParentServiceActivitiesClient creates a new ParentServiceActivitiesClient.
func NewParentServiceActivitiesClient(cli ParentServiceClient) ParentServiceActivitiesClient {
	RegisterParentServiceActivities(cli)
	return ParentServiceActivitiesClient{}
}

func (ca *ParentServiceActivitiesClient) ValidateParent(ctx workflow.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceValidateParentActivity, in)
	var result ValidateParentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
