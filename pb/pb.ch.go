package pb

import (
	context "context"
	x "database/sql"

	sqrl "github.com/elgris/sqrl"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	chaku_globals "go.saastack.io/chaku/chaku-globals"
	driver "go.saastack.io/chaku/driver"
	sql "go.saastack.io/chaku/driver/pgsql"
	errors "go.saastack.io/chaku/errors"
)

var objectTableMap = chaku_globals.ObjectTable{
	"task": {
		"id":                "task",
		"title":             "task",
		"due_time":          "task",
		"notification_type": "task",
		"assignee":          "task",
		"task_status":       "task",
		"priority":          "task",
	},
}

func (m *Task) PackageName() string {
	return "saastack_tasks_v1"
}

func (m *Task) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *Task) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Task) ObjectName() string {
	return "task"
}

func (m *Task) Fields() []string {
	return []string{
		"id", "title", "due_time", "notification_type", "assignee", "task_status", "priority",
	}
}

func (m *Task) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *Task) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Task) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "title":
		return m.Title, nil
	case "due_time":
		return m.DueTime, nil
	case "notification_type":
		return m.NotificationType, nil
	case "assignee":
		return m.Assignee, nil
	case "task_status":
		return m.TaskStatus, nil
	case "priority":
		return m.Priority, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Task) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "title":
		return &m.Title, nil
	case "due_time":
		return &m.DueTime, nil
	case "notification_type":
		return &m.NotificationType, nil
	case "assignee":
		return &m.Assignee, nil
	case "task_status":
		return &m.TaskStatus, nil
	case "priority":
		return &m.Priority, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Task) New(field string) error {
	switch field {
	case "id":
		return nil
	case "title":
		return nil
	case "due_time":
		if m.DueTime == nil {
			m.DueTime = &timestamp.Timestamp{Seconds: -62135596800}
		}
		return nil
	case "notification_type":
		return nil
	case "assignee":
		return nil
	case "task_status":
		return nil
	case "priority":
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Task) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "title":
		return "string"
	case "due_time":
		return "timestamp"
	case "notification_type":
		return "enum"
	case "assignee":
		return "string"
	case "task_status":
		return "enum"
	case "priority":
		return "bool"
	default:
		return ""
	}
}

func (_ *Task) GetEmptyObject() (m *Task) {
	m = &Task{}
	return
}

func (m *Task) GetPrefix() string {
	return "tas"
}

func (m *Task) GetID() string {
	return m.Id
}

func (m *Task) SetID(id string) {
	m.Id = id
}

func (m *Task) IsRoot() bool {
	return true
}

func (m *Task) IsFlatObject(f string) bool {
	return false
}

func (m *Task) NoOfParents(d driver.Descriptor) int {
	switch d.ObjectName() {
	}
	return 0
}

type TaskStore struct {
	d      driver.Driver
	withTx bool
	tx     driver.Transaction

	limitMultiplier int
}

func (s TaskStore) Execute(ctx context.Context, query string, args ...interface{}) error {
	if s.withTx {
		return s.tx.Execute(ctx, query, args...)
	}
	return s.d.Execute(ctx, query, args...)
}

func (s TaskStore) QueryRows(ctx context.Context, query string, scanners []string, args ...interface{}) (driver.Result, error) {
	if s.withTx {
		return s.tx.QueryRows(ctx, query, scanners, args...)
	}
	return s.d.QueryRows(ctx, query, scanners, args...)
}

func NewTaskStore(d driver.Driver) TaskStore {
	return TaskStore{d: d, limitMultiplier: 1}
}

func NewPostgresTaskStore(db *x.DB, usr driver.IUserInfo) TaskStore {
	return TaskStore{
		d:               &sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
		limitMultiplier: 1,
	}
}

type TaskTx struct {
	TaskStore
}

func (s TaskStore) BeginTx(ctx context.Context) (*TaskTx, error) {
	tx, err := s.d.BeginTx(ctx)
	if err != nil {
		return nil, err
	}
	return &TaskTx{
		TaskStore: TaskStore{
			d:      s.d,
			withTx: true,
			tx:     tx,
		},
	}, nil
}

func (tx *TaskTx) Commit(ctx context.Context) error {
	return tx.tx.Commit(ctx)
}

func (tx *TaskTx) RollBack(ctx context.Context) error {
	return tx.tx.RollBack(ctx)
}

func (s TaskStore) CreateTaskPGStore(ctx context.Context) error {
	const queries = `
CREATE SCHEMA IF NOT EXISTS saastack_tasks_v1;
CREATE TABLE IF NOT EXISTS  saastack_tasks_v1.task( id text DEFAULT ''::text , title text DEFAULT ''::text , due_time timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone , notification_type integer DEFAULT 0 , assignee text DEFAULT ''::text , task_status integer DEFAULT 0 , priority boolean DEFAULT false , parent text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, parent)); 
CREATE TABLE IF NOT EXISTS  saastack_tasks_v1.task_parent( id text DEFAULT ''::text , parent text DEFAULT ''::text ); 
`
	if err := s.d.Execute(ctx, queries); err != nil {
		return err
	}
	return nil
}

func (s TaskStore) CreateTasks(ctx context.Context, list ...*Task) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &Task{}, &Task{}, "", []string{})
	}
	return s.d.Insert(ctx, vv, &Task{}, &Task{}, "", []string{})
}

func (s TaskStore) DeleteTask(ctx context.Context, cond TaskCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{})
	}
	return s.d.Delete(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{})
}

func (s TaskStore) UpdateTask(ctx context.Context, req *Task, fields []string, cond TaskCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.taskCondToDriverTaskCond(s.d), req, &Task{}, fields...)
	}
	return s.d.Update(ctx, cond.taskCondToDriverTaskCond(s.d), req, &Task{}, fields...)
}

func (s TaskStore) UpdateTaskMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &Task{}, &Task{}, list...)
}

func (s TaskStore) GetTask(ctx context.Context, fields []string, cond TaskCondition, opt ...getTasksOption) (*Task, error) {
	if len(fields) == 0 {
		fields = (&Task{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listTasksOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListTasks(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s TaskStore) ListTasks(ctx context.Context, fields []string, cond TaskCondition, opt ...listTasksOption) ([]*Task, error) {
	if len(fields) == 0 {
		fields = (&Task{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetTaskCondition == nil {
					page.SetTaskCondition = defaultSetTaskCondition
				}
				cond = page.SetTaskCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*Task, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &Task{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperTask(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s TaskStore) CountTasks(ctx context.Context, cond TaskCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{})
}

type getTasksOption interface {
	getOptTasks() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptTasks() { // method of no significant use
}

type listTasksOption interface {
	listOptTasks() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptTasks() {
}

func (OrderBy) listOptTasks() {
}

func (*CursorBasedPagination) listOptTasks() {
}

func defaultSetTaskCondition(upOrDown bool, cursor string, cond TaskCondition) TaskCondition {
	if upOrDown {
		if cursor != "" {
			return TaskAnd{cond, TaskIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return TaskAnd{cond, TaskIdGt{cursor}}
	}
	return cond
}

type TaskAnd []TaskCondition

func (p TaskAnd) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.taskCondToDriverTaskCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type TaskOr []TaskCondition

func (p TaskOr) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.taskCondToDriverTaskCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type TaskParentEq struct {
	Parent string
}

func (c TaskParentEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentEq struct {
	Parent string
}

func (c TaskFullParentEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentNotEq struct {
	Parent string
}

func (c TaskParentNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentNotEq struct {
	Parent string
}

func (c TaskFullParentNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentLike struct {
	Parent string
}

func (c TaskParentLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentLike struct {
	Parent string
}

func (c TaskFullParentLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentILike struct {
	Parent string
}

func (c TaskParentILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentILike struct {
	Parent string
}

func (c TaskFullParentILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentIn struct {
	Parent []string
}

func (c TaskParentIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentIn struct {
	Parent []string
}

func (c TaskFullParentIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentNotIn struct {
	Parent []string
}

func (c TaskParentNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentNotIn struct {
	Parent []string
}

func (c TaskFullParentNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdEq struct {
	Id string
}

func (c TaskIdEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleEq struct {
	Title string
}

func (c TaskTitleEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimeEq struct {
	DueTime *timestamp.Timestamp
}

func (c TaskDueTimeEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "due_time", Value: c.DueTime, Operator: d, Descriptor: &Task{}, FieldMask: "due_time", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotificationTypeEq struct {
	NotificationType NotificationType
}

func (c TaskNotificationTypeEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Task{}, FieldMask: "notification_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEq struct {
	Assignee string
}

func (c TaskAssigneeEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTaskStatusEq struct {
	TaskStatus TaskStatus
}

func (c TaskTaskStatusEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "task_status", Value: c.TaskStatus, Operator: d, Descriptor: &Task{}, FieldMask: "task_status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskPriorityEq struct {
	Priority bool
}

func (c TaskPriorityEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "priority", Value: c.Priority, Operator: d, Descriptor: &Task{}, FieldMask: "priority", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdNotEq struct {
	Id string
}

func (c TaskIdNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleNotEq struct {
	Title string
}

func (c TaskTitleNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimeNotEq struct {
	DueTime *timestamp.Timestamp
}

func (c TaskDueTimeNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "due_time", Value: c.DueTime, Operator: d, Descriptor: &Task{}, FieldMask: "due_time", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotificationTypeNotEq struct {
	NotificationType NotificationType
}

func (c TaskNotificationTypeNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Task{}, FieldMask: "notification_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeNotEq struct {
	Assignee string
}

func (c TaskAssigneeNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTaskStatusNotEq struct {
	TaskStatus TaskStatus
}

func (c TaskTaskStatusNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "task_status", Value: c.TaskStatus, Operator: d, Descriptor: &Task{}, FieldMask: "task_status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskPriorityNotEq struct {
	Priority bool
}

func (c TaskPriorityNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "priority", Value: c.Priority, Operator: d, Descriptor: &Task{}, FieldMask: "priority", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdGt struct {
	Id string
}

func (c TaskIdGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleGt struct {
	Title string
}

func (c TaskTitleGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimeGt struct {
	DueTime *timestamp.Timestamp
}

func (c TaskDueTimeGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "due_time", Value: c.DueTime, Operator: d, Descriptor: &Task{}, FieldMask: "due_time", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotificationTypeGt struct {
	NotificationType NotificationType
}

func (c TaskNotificationTypeGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Task{}, FieldMask: "notification_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeGt struct {
	Assignee string
}

func (c TaskAssigneeGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTaskStatusGt struct {
	TaskStatus TaskStatus
}

func (c TaskTaskStatusGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "task_status", Value: c.TaskStatus, Operator: d, Descriptor: &Task{}, FieldMask: "task_status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskPriorityGt struct {
	Priority bool
}

func (c TaskPriorityGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "priority", Value: c.Priority, Operator: d, Descriptor: &Task{}, FieldMask: "priority", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdLt struct {
	Id string
}

func (c TaskIdLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleLt struct {
	Title string
}

func (c TaskTitleLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimeLt struct {
	DueTime *timestamp.Timestamp
}

func (c TaskDueTimeLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "due_time", Value: c.DueTime, Operator: d, Descriptor: &Task{}, FieldMask: "due_time", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotificationTypeLt struct {
	NotificationType NotificationType
}

func (c TaskNotificationTypeLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Task{}, FieldMask: "notification_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeLt struct {
	Assignee string
}

func (c TaskAssigneeLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTaskStatusLt struct {
	TaskStatus TaskStatus
}

func (c TaskTaskStatusLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "task_status", Value: c.TaskStatus, Operator: d, Descriptor: &Task{}, FieldMask: "task_status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskPriorityLt struct {
	Priority bool
}

func (c TaskPriorityLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "priority", Value: c.Priority, Operator: d, Descriptor: &Task{}, FieldMask: "priority", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdGtOrEq struct {
	Id string
}

func (c TaskIdGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleGtOrEq struct {
	Title string
}

func (c TaskTitleGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimeGtOrEq struct {
	DueTime *timestamp.Timestamp
}

func (c TaskDueTimeGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "due_time", Value: c.DueTime, Operator: d, Descriptor: &Task{}, FieldMask: "due_time", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotificationTypeGtOrEq struct {
	NotificationType NotificationType
}

func (c TaskNotificationTypeGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Task{}, FieldMask: "notification_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeGtOrEq struct {
	Assignee string
}

func (c TaskAssigneeGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTaskStatusGtOrEq struct {
	TaskStatus TaskStatus
}

func (c TaskTaskStatusGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "task_status", Value: c.TaskStatus, Operator: d, Descriptor: &Task{}, FieldMask: "task_status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskPriorityGtOrEq struct {
	Priority bool
}

func (c TaskPriorityGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "priority", Value: c.Priority, Operator: d, Descriptor: &Task{}, FieldMask: "priority", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdLtOrEq struct {
	Id string
}

func (c TaskIdLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleLtOrEq struct {
	Title string
}

func (c TaskTitleLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimeLtOrEq struct {
	DueTime *timestamp.Timestamp
}

func (c TaskDueTimeLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "due_time", Value: c.DueTime, Operator: d, Descriptor: &Task{}, FieldMask: "due_time", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotificationTypeLtOrEq struct {
	NotificationType NotificationType
}

func (c TaskNotificationTypeLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Task{}, FieldMask: "notification_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeLtOrEq struct {
	Assignee string
}

func (c TaskAssigneeLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTaskStatusLtOrEq struct {
	TaskStatus TaskStatus
}

func (c TaskTaskStatusLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "task_status", Value: c.TaskStatus, Operator: d, Descriptor: &Task{}, FieldMask: "task_status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskPriorityLtOrEq struct {
	Priority bool
}

func (c TaskPriorityLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "priority", Value: c.Priority, Operator: d, Descriptor: &Task{}, FieldMask: "priority", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdLike struct {
	Id string
}

func (c TaskIdLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleLike struct {
	Title string
}

func (c TaskTitleLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeLike struct {
	Assignee string
}

func (c TaskAssigneeLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdILike struct {
	Id string
}

func (c TaskIdILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleILike struct {
	Title string
}

func (c TaskTitleILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeILike struct {
	Assignee string
}

func (c TaskAssigneeILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeleted struct {
	IsDeleted bool
}

func (c TaskDeleted) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByEq struct {
	By string
}

func (c TaskCreatedByEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByNotEq struct {
	By string
}

func (c TaskCreatedByNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByGt struct {
	By string
}

func (c TaskCreatedByGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByLt struct {
	By string
}

func (c TaskCreatedByLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByGtOrEq struct {
	By string
}

func (c TaskCreatedByGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByLtOrEq struct {
	By string
}

func (c TaskCreatedByLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByLike struct {
	By string
}

func (c TaskCreatedByLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByILike struct {
	By string
}

func (c TaskCreatedByILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByEq struct {
	By string
}

func (c TaskUpdatedByEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByNotEq struct {
	By string
}

func (c TaskUpdatedByNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByGt struct {
	By string
}

func (c TaskUpdatedByGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByLt struct {
	By string
}

func (c TaskUpdatedByLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByGtOrEq struct {
	By string
}

func (c TaskUpdatedByGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByLtOrEq struct {
	By string
}

func (c TaskUpdatedByLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByLike struct {
	By string
}

func (c TaskUpdatedByLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByILike struct {
	By string
}

func (c TaskUpdatedByILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByEq struct {
	By string
}

func (c TaskDeletedByEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByNotEq struct {
	By string
}

func (c TaskDeletedByNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByGt struct {
	By string
}

func (c TaskDeletedByGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByLt struct {
	By string
}

func (c TaskDeletedByLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByGtOrEq struct {
	By string
}

func (c TaskDeletedByGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByLtOrEq struct {
	By string
}

func (c TaskDeletedByLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByLike struct {
	By string
}

func (c TaskDeletedByLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByILike struct {
	By string
}

func (c TaskDeletedByILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdIn struct {
	Id []string
}

func (c TaskIdIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleIn struct {
	Title []string
}

func (c TaskTitleIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimeIn struct {
	DueTime []*timestamp.Timestamp
}

func (c TaskDueTimeIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "due_time", Value: c.DueTime, Operator: d, Descriptor: &Task{}, FieldMask: "due_time", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotificationTypeIn struct {
	NotificationType []NotificationType
}

func (c TaskNotificationTypeIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Task{}, FieldMask: "notification_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIn struct {
	Assignee []string
}

func (c TaskAssigneeIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTaskStatusIn struct {
	TaskStatus []TaskStatus
}

func (c TaskTaskStatusIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "task_status", Value: c.TaskStatus, Operator: d, Descriptor: &Task{}, FieldMask: "task_status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskPriorityIn struct {
	Priority []bool
}

func (c TaskPriorityIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "priority", Value: c.Priority, Operator: d, Descriptor: &Task{}, FieldMask: "priority", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdNotIn struct {
	Id []string
}

func (c TaskIdNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleNotIn struct {
	Title []string
}

func (c TaskTitleNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimeNotIn struct {
	DueTime []*timestamp.Timestamp
}

func (c TaskDueTimeNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "due_time", Value: c.DueTime, Operator: d, Descriptor: &Task{}, FieldMask: "due_time", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotificationTypeNotIn struct {
	NotificationType []NotificationType
}

func (c TaskNotificationTypeNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Task{}, FieldMask: "notification_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeNotIn struct {
	Assignee []string
}

func (c TaskAssigneeNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTaskStatusNotIn struct {
	TaskStatus []TaskStatus
}

func (c TaskTaskStatusNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "task_status", Value: c.TaskStatus, Operator: d, Descriptor: &Task{}, FieldMask: "task_status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskPriorityNotIn struct {
	Priority []bool
}

func (c TaskPriorityNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "priority", Value: c.Priority, Operator: d, Descriptor: &Task{}, FieldMask: "priority", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

func (c TrueCondition) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type taskMapperObject struct {
	id               string
	title            string
	dueTime          *timestamp.Timestamp
	notificationType NotificationType
	assignee         string
	taskStatus       TaskStatus
	priority         bool
}

func (s *taskMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperTask(rows []*Task) []*Task {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedTaskMappers := map[string]*taskMapperObject{}

	for _, rw := range rows {

		tempTask := &taskMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempTask.id = rw.Id
		tempTask.title = rw.Title
		tempTask.dueTime = rw.DueTime
		tempTask.notificationType = rw.NotificationType
		tempTask.assignee = rw.Assignee
		tempTask.taskStatus = rw.TaskStatus
		tempTask.priority = rw.Priority

		if combinedTaskMappers[tempTask.GetUniqueIdentifier()] == nil {
			combinedTaskMappers[tempTask.GetUniqueIdentifier()] = tempTask
		}
	}

	combinedTasks := make(map[string]*Task, 0)

	for _, task := range combinedTaskMappers {
		tempTask := &Task{}
		tempTask.Id = task.id
		tempTask.Title = task.title
		tempTask.DueTime = task.dueTime
		tempTask.NotificationType = task.notificationType
		tempTask.Assignee = task.assignee
		tempTask.TaskStatus = task.taskStatus
		tempTask.Priority = task.priority

		if tempTask.Id == "" {
			continue
		}

		combinedTasks[tempTask.Id] = tempTask

	}
	list := make([]*Task, 0, len(combinedTasks))
	for _, i := range ids {
		list = append(list, combinedTasks[i])
	}
	return list
}

func (m *Task) IsUsedMultipleTimes(f string) bool {
	return false
}

type TrueCondition struct{}

type TaskCondition interface {
	taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner
}

type CursorBasedPagination struct {
	// Set UpOrDown = true for getting list of data above Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	// Set UpOrDown = false for getting list of data below Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	Cursor   string
	Limit    int
	UpOrDown bool

	// All pagination-cursor condition functions for different objects
	// SetTaskCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetTaskCondition func(upOrDown bool, cursor string, cond TaskCondition) TaskCondition

	// Response objects Items - will be updated and set after the list call
	HasNext     bool // Used in case of UpOrDown = false
	HasPrevious bool // Used in case of UpOrDown = true
}

func (p *CursorBasedPagination) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_Pagination, p
}

type MetaInfo driver.MetaInfo
type MetaInfoForList []*driver.MetaInfo

func (p *MetaInfo) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

func (p *MetaInfoForList) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

type OrderBy struct {
	Bys []driver.OrderByType
}

func (o OrderBy) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_OrderBy, o
}
