package tasks

import (
	"context"
	"strings"
	"go.saastack.io/idutil"
	"google.golang.org/genproto/protobuf/field_mask"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	activityLogPb "go.saastack.io/activity-log/pb"
	activityLog "go.saastack.io/activity-log"
	"go.saastack.io/task/pb"
	"go.uber.org/zap"
)

type logsTasksServer struct {
	pb.TasksServer
	actLogCli activityLogPb.ActivityLogsClient
}

func NewLogsTasksServer(
	a activityLogPb.ActivityLogsClient,
	s pb.TasksServer,
	str pb.TaskStore,
) pb.TasksServer {

	srv := &logsTasksServer{s, a, str}

	return srv
}

func (s *logsTasksServer) CreateTask(ctx context.Context, in *pb.CreateTaskRequest) (*pb.Task, error) {

	res, err := s.TasksServer.CreateTask(ctx, in)
	if err != nil {
		return nil, err
	}


	task := &pb.Task{}
	if err = task.Update(res, []string{string(pb.Task_Title),string(pb.Task_Status),string(pb.Task_Assignee)}); err != nil {
		return nil, err
	}

	if activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          in.GetParent(res.Id),
		Data:            task,
		EventName:       ".saastack.tasks.v1.Tasks.CreateTask",
		ActivityId:      res.Id,
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) GetTask(ctx context.Context, in *pb.GetTaskRequest) (*pb.Task, error) {

	res, err := s.TasksServer.GetTask(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) DeleteTask(ctx context.Context, in *pb.DeleteTaskRequest) (*empty.Empty, error) {

	tas, err := s.str.GetTask(ctx, []string{
		string(pb.Task_Id),
	}, pb.TaskIdEq{Id: in.Id})
	if err != nil {
		return nil, err
	}

	res, err := s.TasksServer.DeleteTask(ctx, in)
	if err != nil {
		return nil, err
	}

	/*
		Template: {{id}}
	*/
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(in.Id),
		Data:            tas,
		EventName:       ".saastack.tasks.v1.Tasks.DeleteCustomer",
		ActivityId:      in.Id,
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) UpdateTask(ctx context.Context, in *pb.UpdateTaskRequest) (*pb.Task, error) {

	tas, err := s.str.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.GetTask().GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.TasksServer.UpdateTask(ctx, in)
	if err != nil {
		return nil, err
	}

	masks := pb.TaskObjectCompare(tas, res, "")
	objMask := []string{}
	for _, oldMask := range masks {
		if strings.Contains(oldMask, "address") || strings.Contains(oldMask, "gallery") {
			continue
		}
		objMask = append(objMask, oldMask)
	}

	new := &pb.Task{}
	if err = new.Update(res, objMask); err != nil {
		return nil, err
	}
	old := &pb.Task{}
	if err = old.Update(tas, objMask); err != nil {
		return nil, err
	}

	updateTaskLog := &pb.UpdateTaskLog{
		OldTask: old,
		NewTask: new,
		UpdateMask:  &field_mask.FieldMask{Paths: masks},
	}

	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(res.GetId()),
		Data:            updateTaskLog,
		EventName:       ".saastack.tasks.v1.Tasks.UpdateTask",
		ActivityId:      res.GetId(),
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        make(map[string]string, 0),
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) ListTask(ctx context.Context, in *pb.ListTaskRequest) (*pb.ListTaskResponse, error) {

	res, err := s.TasksServer.ListTask(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) UpdateAssignedEmployee(ctx context.Context, in *pb.UpdateAssignedEmployeeRequest) (*pb.Task, error) {

	res, err := s.TasksServer.UpdateAssignedEmployee(ctx, in)
	if err != nil {
		return nil, err
	}

	tas := &pb.Task{}
	if err = tas.Update(res, pb.UpdateAssignedEmployeeRequest_EmpId(
		pb.UpdateAssignedEmployeeRequest_EmpId,
	)); err != nil {
		return nil, err
	}

	/*
		Template: employee id
	*/
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(res.GetId()),
		Data:            tas,
		EventName:       ".saastack.tasks.v1.Tasks.UpdateAssignedEmployee",
		ActivityId:      res.Id,
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil

}

func (s *logsTasksServer) TasksReport(ctx context.Context, in *pb.TasksReportRequest) (*pb.TasksReportResponse, error) {

	res, err := s.TasksServer.TasksReport(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}
