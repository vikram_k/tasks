package tasks

import (
	"context"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"go.saastack.io/jaal/schemabuilder"
	"go.saastack.io/task/pb"
	"go.uber.org/fx"
	"google.golang.org/grpc"
)

// TODO := Replace "Books" with your service-name
// TODO := Replace "Book" with your object-name

// Module is the fx module encapsulating all the providers of the package
var Module = fx.Options(
	fx.Provide(
		pb.NewPostgresTaskStore,
		NewParentServiceServer,
		pb.NewLocalParentServiceClient,

		NewTasksServer,
		fx.Annotated{
			Name: "public",
			Target: func(srv pb.TasksServer) pb.TasksServer {
				return srv
			},
		},

		pb.NewLocalTasksClient,
		fx.Annotated{
			Name: "public",
			Target: func(in struct {
				fx.In
				S pb.TasksServer `name:"public"`
			}) pb.TasksClient {
				return pb.NewLocalTasksClient(in.S)
			},
		},

		fx.Annotated{
			Group:  "grpc-service",
			Target: RegisterGRPCService,
		},
		fx.Annotated{
			Group:  "graphql-service",
			Target: RegisterGraphQLService,
		},
		fx.Annotated{
			Group:  "http-service",
			Target: RegisterHttpService,
		},
	),
	fx.Decorate(
		pb.NewEventsTasksServer,
	),
)

func RegisterGRPCService(in struct {
	fx.In
	Server pb.TasksServer `name:"public"`
}) func(s *grpc.Server) {
	return func(s *grpc.Server) {
		pb.RegisterTasksServer(s, in.Server)
	}
}

func RegisterGraphQLService(in struct {
	fx.In
	Client pb.TasksClient `name:"public"`
}) func(s *schemabuilder.Schema) {
	return func(s *schemabuilder.Schema) {
		pb.RegisterTasksOperations(s, in.Client)
	}
}

func RegisterHttpService(in struct {
	fx.In
	Client pb.TasksClient `name:"public"`
}) func(*runtime.ServeMux, context.Context) error {
	return func(mux *runtime.ServeMux, ctx context.Context) error {
		return pb.RegisterTasksHandlerClient(ctx, mux, in.Client)
	}
}

